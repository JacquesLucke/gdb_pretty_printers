Add the following line to a `~/.gdbinit` file:

```
source /path/to/this/repo/blenlib.py
```

To get the frame decorator to work in vscode, a few steps are necessary right now:

- Replace `fullname` with `file` in `~/.vscode/extensions/webfreak.debug-0.27.0/out/src/backend/mi2/mi2.js` (that's the "Native Debug" extension).
  - Without this, the source locations won't work properly. Not sure why gdb does not provide the `fullname` when frame filters are enabled yet.
- After debugging started, run `-enable-frame-filters` in the Debug Console in vscode (step forwards afterwards, to reload the backtrace).
